<?php
/**
 * Uninstall routine
 *
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

// Delete the options.
delete_option( 'g_smtp_settings' );
delete_option( 'g_smtp_password' );
delete_site_option( 'g_smtp_settings' );
delete_site_option( 'g_smtp_password' );

// Remove the cron event.
$timestamp = wp_next_scheduled( 'g_smtp_verify_rights_cron_hook' );
wp_unschedule_event( $timestamp, 'g_smtp_verify_rights_cron_hook' );
