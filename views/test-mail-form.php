<?php
/**
 * Test mail form
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

/**
 * Renders the test mail form
 *
 * @return string
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.1.0
 * @since 1.6.0
 */
function test_mail_form() {
	if ( isset( $_GET['test-mail-reciever'] ) ) {
		$test_mail_reciever = sanitize_email( wp_unslash( $_GET['test-mail-reciever'] ) );
	} else {
		$test_mail_reciever = '';
	}
	?>

	<form action="" method="post">
		<h2><?php esc_html_e( 'Test mail', 'gital-smtp' ); ?></h2>
		<?php
		if ( isset( $_GET['test-mail-sent-success'] ) ) {
			if ( 'true' === $_GET['test-mail-sent-success'] ) {
				?>
				<div style="margin-left: 0;" id="message" class="notice notice-success is-dismissible">
					<p><?php echo esc_html__( 'Your test mail has been sent.', 'gital-smtp' ); ?>
					</p>
				</div>
				<?php
			} else {
				?>
				<div style="margin-left: 0;" id="message" class="notice notice-warning is-dismissible">
					<p><?php echo esc_html__( 'Your test mail has not been sent.', 'gital-smtp' ); ?>
					</p>
				</div>
				<?php
			}
		}
		?>
		<p><?php echo esc_html__( 'Send test mail to verify that the settings works as expected', 'gital-smtp' ); ?></p>
		<table class="form-table" role="presentation">
			<tbody>
				<tr>
					<th scope="row">E-Post</th>
					<td><input type="email" name="email" value="<?php echo esc_attr( $test_mail_reciever ); ?>">
						<p><i><?php echo esc_html__( 'Leave empty to send to webb@gibon.se', 'gital-smtp' ); ?></i></p>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="submit"><input name="test-mail" class="button button-primary" type="submit" value=" <?php echo esc_html__( 'Send mail', 'gital-smtp' ); ?>" /></div>
		<input type="hidden" name="button_pressed" value="1" />
	</form>
	<?php
}
