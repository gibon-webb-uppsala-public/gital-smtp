=== Gital SMTP ===
Contributors: gibonadmin
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Gital SMTP is made with passion in Uppsala, Sweden. The plugin is adding support for the Gibon SMTP server for outgoing mail traffic to your site. If you\'d like support, please contact us at webb@gibon.se.

== Description ==
The Gital SMTP is made with passion in Uppsala, Sweden. The plugin is adding support for the Gibon SMTP server for outgoing mail traffic to your site. If you\'d like support, please contact us at webb@gibon.se.

== Installation ==
1. Add the plugin and add the settings given by Gibon to the config file or the env. and application file if on a Bedrock installation.
2. Set the password in the database by using the set_password function.

== Changelog ==

= 5.1.0 - 2024.09.04 =
* The plugin now utilizes the new repo.

= 5.0.4 =
* Update: The plugin now uses the Settings class.
* Update: New log function.
* Update: Better test mail.

= 4.3.1 =
* Bugfix: class-mail-init.php had a misspelling of wordpress causing it to always use the default name.

= 4.3.0 =
* Update: class-mail-init.php is now using str_contains().
* Bugfix: class-mail-init.php had a misspelling of wordpress causing it to always use the default address.

= 4.2.0 =
* Update: Increased security with escape functions.

= 4.1.0 =
* Update: Verify_Password->get_verbose_status is now also detecting if the key is not set.
* Update: The verify rights cron event removal on deactivation is moved to uninstall.php.
* Update: The verify rights cron event is now running every day.
* Update: The plugin only retrieves the password when it needs to.
* Update: Removed the G_SMTP_PASS constant.

= 4.0.1 =
* Bugfix: The definement of G_SMTP_PASS always assumed that the option g_smtp_password where set.

= 4.0.0 =
* Update: Changed SMTP-user and password.
* Update: Added password encryption.

= 3.0.3 =
* Update: Switched Webb Uppsala to just Webb.
* Update: Added multiple new constants.
* Update: Removed the function g_smtp_send_test_mail in favour for gital_smtp\send_test_mail.
* Update: Removed the function g_smtp_verify_password.
* Update: Added php linter and formater.
* Update: Updated the updater to v5.
* Bugfix: Updated the function send_test_mail with correct methods.

= 2.2.2 =
* Bugfix: An undefined key constant caused an fatal error.

= 2.2.1 =
* Update: Added the Local By Flywheel server IP to the allowed servers array.

= 2.1.6 =
* Update: Added the new server IP to the allowed servers array.

= 2.1.5 =
* Update: Fixed translations

= 2.1.3 =
* Update: Added cron event to weekly control if the server is verified to use the plugin.

= 2.0.0 =
* Update: Cleaned up the plugin and went over to the Wordpress standrad.

= 1.9.0 =
* Update: The plugin now overrides if the admin e-mail is used.

= 1.8.0 =
* Update: Now, the from e-mail and from name only override the incoming if they are set to the default values.

= 1.7.2 =
* Bugfix: Function get_verbose_status in class G_SmtpVerifyPassword did not verify that the constant SMTP_PASS was defined

= 1.7.1 =
* Bugfix: Function get_verbose_status in class G_SmtpVerifyPassword did not return the correct value

= 1.7.0 =
* Update: Removed ability to manually add the password from wp-admin
* Update: Added verification of the password
* Update: g_smtp_send_test_mail now return a status

= 1.6.2 =
* Update: g_smtp_send_test_mail now returns status of the sent mail

= 1.6.1 =
* Update: Added translations to Swedish since 1.5.0

= 1.6.0 =
* Update: Added functionality to get a debug log
* Update: Added GUI to g_Smtp_Test_Mail

= 1.5.0 =
* Update: Switched from functions to classes and methods
* Update: Added g_Smtp_Test_Mail class to be able to send test mails. This is done by initiating the function g_smtp_send_test_mail()

= 1.4.3 =
* Bugfix: $mail was defined as variable

= 1.4.2 =
* Bugfix: mail_init instead of phpmailer_init because of search/replace

= 1.4.1 =
* Update: Better documentation

= 1.3.5 =
* Update: Backup to $options['g_smtp_settings_name'] and $options['g_smtp_settings_email'] if not defined

= 1.2.0 =
* Update: Moved plugin-update-checker to composer

= 1.1.6 =
* Bugfix: If the password where not defined by the define('SMTP_PASS'...), the mail would not be sent

= 1.1.5 =
* add_action('plugins_loaded'...) instead of add_action('init'...)

= 1.1.4 =
* Bugfix

= 1.1.3 =
* Added composer support

= 1.1.2 =
* Bugfix

= 1.1.1 =
* Bugfix

= 1.1.0 =
* Added a better password handling system
* Updated translations

= 1.0.9 =
* Bugfix

= 1.0.8 =
* Bugfix

= 1.0.7 =
* Bugfix

= 1.0.6 =
* Switched updater

= 1.0.5 =
* Bugfixes

= 1.0.4 =
* Bugfixes

= 1.0.3 =
* Bugfixes

= 1.0.2 =
* Removed several values from the config and only left out the password

= 1.0.1 =
* Test

= 1.0.0 =
* Added translations and finilizing the plugin

= 0.0.19 =
* Bugfixes

= 0.0.18 =
* Bugfixes

= 0.0.17 =
* Bugfixes

= 0.0.16 =
* Bugfixes

= 0.0.15 =
* Bugfixes

= 0.0.14 =
* Moved to vanilla fields

= 0.0.13 =
* Bugfixes

= 0.0.12 =
* Bugfixes

= 0.0.11 =
* Bugfixes

= 0.0.10 =
* Bugfixes

= 0.0.9 =
* Bugfixes

= 0.0.8 =
* Testing the updater

= 0.0.7 =
* Added updater feature

= 0.0.6 =
* Added internationalization

= 0.0.5 =
* Insted of forcing a From and FromName, the user can now leave it blank

= 0.0.4 =
* Corrected bug that made the name not being used

= 0.0.3 =
* Added fallback for name and domain

= 0.0.2 =
* Added settings page

= 0.0.1 =
* Init