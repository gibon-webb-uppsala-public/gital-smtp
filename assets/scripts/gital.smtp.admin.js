document
	.getElementById('g_smtp_clear_log_button')
	.addEventListener('click', function () {
		if (confirm(gital_smtp_admin.are_you_sure)) {
			fetch(gital_smtp_admin.clear_log_route, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'X-WP-Nonce': gital_smtp_admin.nonce,
				},
			})
				.then((response) => response.json())
				.then((data) => {
					alert(data.message);
					location.reload();
				})
				.catch((error) => {
					console.error('Error:', error);
				});
		}
	});
