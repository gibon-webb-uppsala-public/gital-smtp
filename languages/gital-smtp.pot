#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gital SMTP\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-08-23 08:24+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.4.0; wp-5.4.2"

#: classes/class-mail-logger.php:61
msgid "Are you sure you want to clear the log?"
msgstr ""

#: classes/class-option-page.php:171 classes/class-option-page.php:234
msgid "Clear log"
msgstr ""

#: classes/class-option-page.php:172
msgid ""
"Clear the log from all outgoing mails. If the logging is not enabled, the "
"database table will be removed completly."
msgstr ""

#: classes/class-mail-logger.php:105
msgid "Database table removed successfully."
msgstr ""

#: classes/class-mail-logger.php:298
msgid "Date"
msgstr ""

#: classes/class-option-page.php:202
msgid "E-Mail"
msgstr ""

#: classes/class-option-page.php:51 classes/class-option-page.php:194
msgid "E-Mail (SMTP)"
msgstr ""

#: classes/class-option-page.php:86
msgid "Enter the settings for the outgoing mailtraffic"
msgstr ""

#: classes/class-test-mail.php:47
msgid "Full mail object"
msgstr ""

#. Author of the plugin
msgid "Gibon Webb"
msgstr ""

#. Name of the plugin
msgid "Gital SMTP"
msgstr ""

#: classes/class-test-mail.php:43
msgid "Host:"
msgstr ""

#: classes/class-mail-logger.php:294
msgid "Hover over each row to see the headers."
msgstr ""

#. Author URI of the plugin
msgid "https://www.gibon.se/it-tjanster/webbproduktion/"
msgstr ""

#: classes/class-option-page.php:116
msgid ""
"Leave empty to fall back on the WordPress default. If a name is set in a "
"form or another service, that name will become prioritized."
msgstr ""

#: classes/class-option-page.php:101
msgid ""
"Leave empty to fall back on the WordPress default. If an address is set in a "
"form or another service, that address will become prioritized."
msgstr ""

#: views/test-mail-form.php:55
msgid "Leave empty to send to webb@gibon.se"
msgstr ""

#: classes/class-option-page.php:226
msgid "Log"
msgstr ""

#: classes/class-mail-logger.php:110
msgid "Log cleared successfully."
msgstr ""

#: classes/class-option-page.php:158
msgid "Log outgoing mail traffic."
msgstr ""

#: classes/class-test-mail.php:42
msgid "Mail settings"
msgstr ""

#: classes/class-mail-logger.php:301
msgid "Message"
msgstr ""

#: classes/class-option-page.php:210
msgid "Name"
msgstr ""

#: classes/class-mail-logger.php:290
msgid "No emails have been sent yet."
msgstr ""

#: classes/class-mail-logger.php:287
msgid "Outgoing Email Log"
msgstr ""

#: classes/class-option-page.php:218
msgid "Password"
msgstr ""

#: classes/class-test-mail.php:45
msgid "Password status:"
msgstr ""

#: gital-smtp.php:56
msgid ""
"Please enable <strong>Gital Library</strong> in order to use Gital SMTP."
msgstr ""

#: classes/class-test-mail.php:46
msgid "Port:"
msgstr ""

#: views/test-mail-form.php:60
msgid "Send mail"
msgstr ""

#: views/test-mail-form.php:49
msgid "Send test mail to verify that the settings works as expected"
msgstr ""

#: classes/class-option-page.php:85
msgid "Settings"
msgstr ""

#: classes/class-mail-logger.php:300
msgid "Subject"
msgstr ""

#: classes/class-test-mail.php:69
msgid "Test email"
msgstr ""

#: views/test-mail-form.php:29
msgid "Test mail"
msgstr ""

#: classes/class-test-mail.php:69
msgid "Testing sending email from"
msgstr ""

#. Description of the plugin
msgid ""
"The Gital SMTP is made with passion in Uppsala, Sweden. The plugin is adding "
"support for the Gibon SMTP server for outgoing mail traffic to your site. If "
"you'd like support, please contact us at webb@gibon.se."
msgstr ""

#: classes/class-verify-password.php:41
msgid "The password is not set"
msgstr ""

#: classes/class-verify-password.php:48
msgid "The password is set and correct"
msgstr ""

#: classes/class-verify-password.php:50
msgid "The password is set but not correct"
msgstr ""

#: classes/class-verify-password.php:37
msgid "The password key is not set"
msgstr ""

#: classes/class-mail-logger.php:299
msgid "To"
msgstr ""

#: classes/class-test-mail.php:44
msgid "Username:"
msgstr ""

#: views/test-mail-form.php:35
msgid "Your test mail has been sent."
msgstr ""

#: views/test-mail-form.php:42
msgid "Your test mail has not been sent."
msgstr ""
