��    *      l      �      �  '   �  	   �  s   �  $   c     �     �     �  /   �     �     �  '   �  �     �   �  $   "     G     K     e     �     �     �     �     �     �     �  H   �     /  	   5  <   ?     |     �  
   �  	   �     �     �     �  #   �          5  	   8     B  !   `  �  �  +   	  	   7	  �   A	  ,   �	     �	     �	     
  /   
     C
     U
  8   \
  �   �
  �     .   �     �  !   �     �       
   +     6  #   ;     _  	   x     �  U   �     �     �  P   �     O     ^     d     m     y     �  #   �  (   �  "   �          !  !   0  &   R   Are you sure you want to clear the log? Clear log Clear the log from all outgoing mails. If the logging is not enabled, the database table will be removed completly. Database table removed successfully. Date E-Mail E-Mail (SMTP) Enter the settings for the outgoing mailtraffic Full mail object Host: Hover over each row to see the headers. Leave empty to fall back on the WordPress default. If a name is set in a form or another service, that name will become prioritized. Leave empty to fall back on the WordPress default. If an address is set in a form or another service, that address will become prioritized. Leave empty to send to webb@gibon.se Log Log cleared successfully. Log outgoing mail traffic. Mail settings Message Name No emails have been sent yet. Outgoing Email Log Password Password status: Please enable <strong>Gital Library</strong> in order to use Gital SMTP. Port: Send mail Send test mail to verify that the settings works as expected Settings Subject Test email Test mail Testing sending email from The password is not set The password is set and correct The password is set but not correct The password key is not set To Username: Your test mail has been sent. Your test mail has not been sent. Project-Id-Version: Gital SMTP
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-08-17 19:15+0000
PO-Revision-Date: 2024-08-23 08:26+0000
Last-Translator: 
Language-Team: Svenska
Language: sv_SE
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.2; wp-5.5 Är du säker på att du vill rensa loggen? Rensa log Rensa loggen från alla utgående e-postmeddelanden. Om loggning inte är aktiverad kommer databastabellen att tas bort helt och hållet. Databas tabellen är framgångsrikt raderad. Datum E-Post E-Post (SMTP) Ange inställningarna för utgående mailtrafik Hela mailobjektet Värd: För musen över varje rad för att se mailets sidhuvud. Lämna tomt för att istället använda Wordpress standard. Om ett namn är satt i en annan tjänst så kommer det namnet prioriteras. Lämna tomt för att istället använda Wordpress standard. Om en adress är satt i en annan tjänst så kommer den adressen prioriteras. Lämna tomt för att skicka till webb@gibon.se Logg Loggen är framgångsrikt rensad. Logga utgående mailtrafik. Mailinställningar Meddelande Namn Inga mail har blivit skickade ännu Logg för utgående mail Lösenord Lösenordsstatus: Vänligen aktivera <strong>Gital Library</strong> för att kunna använda Gital SMTP. Port: Skicka e-post Skicka testmail för att verifiera att inställningarna fungerar som förväntat Inställningar Ämne Testmail Test e-post Testar skicka mail från Lösenordet är inte satt Lösenordet är angivet och korrekt Lösenordet är angivet men inte korrekt Lösenordsnyckeln är inte angiven Till Användarnamn: Ditt testmeddelande har skickats. Ditt testmeddelande har inte skickats. 