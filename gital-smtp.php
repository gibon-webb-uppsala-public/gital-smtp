<?php
/**
 * Plugin Name: Gital SMTP
 * Author: Gibon Webb
 * Version: 5.1.0
 * Text Domain: gital-smtp
 * Domain Path: /languages/
 *
 * Author URI: https://www.gibon.se/it-tjanster/webbproduktion/
 * Description: The Gital SMTP is made with passion in Uppsala, Sweden. The plugin is adding support for the Gibon SMTP server for outgoing mail traffic to your site. If you'd like support, please contact us at webb@gibon.se.
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}

/**
 * Loads text domain
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 * @since 0.0.2
 */
function load_textdomain() {
	load_plugin_textdomain( 'gital-smtp', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'gital_smtp\load_textdomain', 10 );

/**
 * Check dependencies
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
function check_dependencies() {
	require_once ABSPATH . 'wp-admin/includes/plugin.php';
	if ( ! function_exists( 'deactivate_plugins' ) ) {
		return;
	}

	if ( ! is_plugin_active( 'gital-library/gital-library.php' ) ) {
		deactivate_plugins( 'gital-smtp/gital-smtp.php' );
		add_action(
			'admin_notices',
			function () {
				?>
				<div class="notice error">
					<p>
						<?php wp_kses( _e( 'Please enable <strong>Gital Library</strong> in order to use Gital SMTP.', 'gital-smtp' ), array( 'strong' => array() ) ); ?>
					</p>
				</div>
				<?php
			}
		);
	}
}
add_action( 'plugins_loaded', 'gital_smtp\check_dependencies' );

// Autoloader.
require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

// Define settings.
$settings = Settings::get_instance();

// Init the updater.
require $settings->get( 'path_vendor' ) . 'yahnis-elsts/plugin-update-checker/plugin-update-checker.php';
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;
$myUpdateChecker = PucFactory::buildUpdateChecker(
	'https://packages.gital.se/wordpress/gital-smtp.json',
	__FILE__,
	'gital-smtp'
);

// Require and init classes.
Mail_Logger::get_instance();
Option_Page::get_instance();
Mail_Init::get_instance();
Debug_Log::get_instance();
Verify_Rights::get_instance();

// Require functions.
require_once $settings->get( 'path_functions' ) . 'send-test-mail.php';
require_once $settings->get( 'path_functions' ) . 'set-password.php';
