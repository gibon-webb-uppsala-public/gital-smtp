<?php
/**
 * Test mail
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

if ( ! class_exists( 'Test_Mail' ) ) {
	/**
	 * Test mail
	 *
	 * Send test mail
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.0
	 * @since 1.5.0
	 */
	class Test_Mail {
		public function __construct() {
			add_action( 'phpmailer_init', array( $this, 'add_host_to_body' ), 10 );
		}

		/**
		 * Adds the host, username and port to the body and subject
		 *
		 * @param \PHPMailer\PHPMailer\PHPMailer $mail The PHPMailer object.
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 1.5.0
		 */
		public function add_host_to_body( \PHPMailer\PHPMailer\PHPMailer $mail ): void {
			$mail_without_body = clone $mail;
			unset( $mail_without_body->Body );

			$mail->Body    .= '<br><br><h2>' . esc_html__( 'Mail settings', 'gital-smtp' ) . '</h2>';
			$mail->Body    .= '<br>' . esc_html__( 'Host:', 'gital-smtp' ) . ' ' . $mail->Host;
			$mail->Body    .= '<br>' . esc_html__( 'Username:', 'gital-smtp' ) . ' ' . $mail->Username;
			$mail->Body    .= '<br>' . esc_html__( 'Password status:', 'gital-smtp' ) . ' ' . Verify_Password::get_verbose_status();
			$mail->Body    .= '<br>' . esc_html__( 'Port:', 'gital-smtp' ) . ' ' . $mail->Port;
			$mail->Body    .= '<br><br><h2>' . esc_html__( 'Full mail object', 'gital-smtp' ) . '</h2>';
			$mail->Body    .= '<br><pre>' . print_r( $mail_without_body, true ) . '</pre>';
			$mail->Subject .= ' with ' . $mail->Host;
		}

		/**
		 * Sends the test mail
		 *
		 * @param string $address The mail address to send to.
		 *
		 * @return bool True if the mail was sent, false if not
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.5.0
		 */
		public function send_test_mail( string $address ): bool {
			$title   = get_bloginfo( 'name' );
			$headers = array( 'Content-Type: text/html;charset=UTF-8' );

			if ( is_email( $address ) ) {
				return wp_mail( $address, esc_html__( 'Test email', 'gital-smtp' ) . ' ' . $title, esc_html__( 'Testing sending email from', 'gital-smtp' ) . ' ' . $title, $headers );
			} else {
				return false;
			}
		}
	}
}
