<?php
/**
 * Mail logger
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Singleton;
use function gital_library\parse_args;

if ( ! class_exists( 'Mail_Logger' ) ) {
	/**
	 * Mail_Logger
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.3
	 * @since 1.5.0
	 */
	class Mail_Logger extends Singleton {
		use Options_Handler;

		/**
		 * Initializes the mail logger
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init(): void {
			add_action( 'rest_api_init', array( $this, 'register_routes' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );
		}

		/**
		 * Enqueues the admin scripts
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function enqueue_admin_scripts(): void {
			$settings = Settings::get_instance();

			if ( 'settings_page_g-smtp-settings' !== get_current_screen()->id ) {
				return;
			}

			wp_enqueue_script( 'gital-smtp-admin', $settings->get( 'url_assets' ) . '/scripts/gital.smtp.admin.js', array(), '1.0', true );
			wp_localize_script(
				'gital-smtp-admin',
				'gital_smtp_admin',
				array(
					'clear_log_route' => esc_url( rest_url( 'gital/smtp/v1/clear-log' ) ),
					'nonce'           => wp_create_nonce( 'wp_rest' ),
					'are_you_sure'    => esc_html__( 'Are you sure you want to clear the log?', 'gital-smtp' ),
				)
			);
		}

		/**
		 * Registers the routes
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function register_routes(): void {
			register_rest_route(
				'gital/smtp/v1',
				'/clear-log',
				array(
					'methods'             => 'POST',
					'callback'            => array( $this, 'clear_log_callback' ),
					'permission_callback' => function () {
						return current_user_can( 'manage_options' );
					},
				)
			);
		}

		/**
		 * Clears the log
		 *
		 * @param \WP_REST_Request $request The WP_REST_Request object.
		 *
		 * @return \WP_REST_Response The WP_REST_Response object
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function clear_log_callback( \WP_REST_Request $request ): \WP_REST_Response {
			$options = $this->get_options();

			if ( $options['log'] != 1 ) {
				$this->remove_table();
				return new \WP_REST_Response( array( 'message' => esc_html__( 'Database table removed successfully.', 'gital-smtp' ) ), 200 );
			}

			$this->clear_log();

			return new \WP_REST_Response( array( 'message' => esc_html__( 'Log cleared successfully.', 'gital-smtp' ) ), 200 );
		}

		/**
		 * Clears the log
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		private function clear_log(): void {
			global $wpdb;
			$table_name = $wpdb->prefix . 'g_smtp_mail_log';
			$wpdb->query( "TRUNCATE TABLE $table_name" );
		}

		/**
		 * Removes the table
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		private function remove_table(): void {
			global $wpdb;
			$table_name = $wpdb->prefix . 'g_smtp_mail_log';
			$wpdb->query( "DROP TABLE IF EXISTS $table_name" );
		}

		/**
		 * Checks if the table exists
		 *
		 * @param string $table_name The table name.
		 *
		 * @return bool True if the table exists, false otherwise
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function table_exists( $table_name ): bool {
			global $wpdb;
			$sql    = "SHOW TABLES LIKE '$table_name'";
			$result = $wpdb->get_var( $sql );
			return ! empty( $result );
		}

		/**
		 * Sets up the table
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function setup_table(): void {
			global $wpdb;
			$table_name = $wpdb->prefix . 'g_smtp_mail_log';

			if ( $this->table_exists( $table_name ) ) {
				return;
			}

			$charset_collate = $wpdb->get_charset_collate();

			$sql = "CREATE TABLE $table_name (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
				email_to varchar(100) NOT NULL,
				subject varchar(255) NOT NULL,
				message text NOT NULL,
				headers text,
				attachments text,
				PRIMARY KEY  (id)
			) $charset_collate;";

			require_once ABSPATH . 'wp-admin/includes/upgrade.php';
			dbDelta( $sql );
		}

		/**
		 * Extracts the email addresses
		 *
		 * @param array|string $to_addresses The to addresses.
		 *
		 * @return string The email addresses
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		protected function extract_email_addresses( $to_addresses ): string {
			if ( ! is_array( $to_addresses ) ) {
				return $to_addresses;
			}

			$emails = array();

			foreach ( $to_addresses as $address ) {
				if ( isset( $address[0] ) && is_email( $address[0] ) ) {
					$emails[] = $address[0];
				}
			}

			return implode( ',', $emails );
		}

		/**
		 * Adds a log entry
		 *
		 * @param \PHPMailer\PHPMailer\PHPMailer $mail The PHPMailer object.
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function add_log_entry( \PHPMailer\PHPMailer\PHPMailer $mail ): void {
			global $wpdb;
			$table_name   = $wpdb->prefix . 'g_smtp_mail_log';
			$to_addresses = $this->extract_email_addresses( $mail->getToAddresses() );

			$wpdb->insert(
				$table_name,
				array(
					'time'        => current_time( 'mysql' ),
					'email_to'    => $to_addresses,
					'subject'     => $mail->Subject,
					'message'     => wp_kses( $mail->Body, 'post' ),
					'headers'     => $mail->createHeader(),
					'attachments' => $mail->getAttachments(),
				)
			);
		}

		/**
		 * Gets the log
		 *
		 * @return array The log
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function get_log(): array {
			global $wpdb;
			$table_name = $wpdb->prefix . 'g_smtp_mail_log';

			$sql = "SELECT * FROM $table_name ORDER BY time DESC LIMIT 10";
			return $wpdb->get_results( $sql, ARRAY_A );
		}

		/**
		 * Renders the mail log section
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function mail_log_section(): void {
			$options = $this->get_options();
			if ( ! isset( $options['log'] ) || 1 != $options['log'] ) {
				return;
			}

			$this->setup_table();

			$mail_log = $this->get_log();

			echo '<h2>' . esc_html__( 'Outgoing Email Log', 'gital-smtp' ) . '</h2>';

			if ( empty( $mail_log ) ) {
				echo '<p>' . esc_html__( 'No emails have been sent yet.', 'gital-smtp' ) . '</p>';
				return;
			}

			echo '<p>' . esc_html__( 'Hover over each row to see the headers.', 'gital-smtp' ) . '</p>';
			echo '<table class="wp-list-table widefat fixed striped">';
			echo '<thead>';
			echo '<tr>';
			echo '<th>' . esc_html__( 'Date', 'gital-smtp' ) . '</th>';
			echo '<th>' . esc_html__( 'To', 'gital-smtp' ) . '</th>';
			echo '<th>' . esc_html__( 'Subject', 'gital-smtp' ) . '</th>';
			echo '<th style="width: 60%;">' . esc_html__( 'Message', 'gital-smtp' ) . '</th>';
			echo '</tr>';
			echo '</thead>';
			echo '<tbody>';

			foreach ( $mail_log as $log_entry ) {
				$defaults  = array(
					'time'        => '',
					'email_to'    => '',
					'subject'     => '',
					'message'     => '',
					'headers'     => '',
					'attachments' => '',
				);
				$log_entry = wp_parse_args( $log_entry, $defaults );

				echo '<tr title="' . esc_html( $log_entry['headers'] ) . '">';
				echo '<td>' . esc_html( $log_entry['time'] ) . '</td>';
				echo '<td>' . esc_html( $log_entry['email_to'] ) . '</td>';
				echo '<td>' . esc_html( $log_entry['subject'] ) . '</td>';
				echo '<td style="width: 60%;">' . wp_kses( $log_entry['message'], array( 'br' => array() ) ) . '</td>';
				echo '</tr>';
			}

			echo '</tbody>';
			echo '</table>';
		}
	}
}
