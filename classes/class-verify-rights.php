<?php
/**
 * Verify rights
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Settings;
use gital_library\Singleton;

if ( ! class_exists( 'Verify_Rights' ) ) {
	/**
	 * Verify rights
	 *
	 * Adds a cron event that each week controls that the server has the correct rights to use the plugin
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.2.0
	 * @since 2.1.0
	 */
	class Verify_Rights extends Singleton {
		/**
		 * The library settings
		 *
		 * @var Settings
		 */
		protected $library_settings;

		/**
		 * The currect servers IP-address
		 *
		 * @var string
		 */
		private $server_ip;

		/**
		 * Adds the cron hook and schedules ot weekly
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @since 2.1.0
		 * @version 1.0.0
		 */
		public function init(): void {
			$this->library_settings = Settings::get_instance();

			add_action( 'g_smtp_verify_rights_cron_hook', array( $this, 'execute_actions' ) );
			if ( ! wp_next_scheduled( 'g_smtp_verify_rights_cron_hook' ) ) {
				wp_schedule_event( time(), 'daily', 'g_smtp_verify_rights_cron_hook' );
			}
		}

		/**
		 * Get allowed servers
		 *
		 * @return array Returns the allowed servers
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.1
		 * @since 2.1.0
		 */
		public function get_allowed_servers(): array {
			return $this->library_settings->get( 'allowed_servers' );
		}

		/**
		 * Control if the server is unauthorized
		 *
		 * @return bool Returns true if the server is unauthorized
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 * @since 2.1.0
		 */
		public function is_unauthorized(): bool {
			if ( isset( $_SERVER['SERVER_ADDR'] ) && ! empty( $_SERVER['SERVER_ADDR'] ) ) {
				$this->server_ip = $_SERVER['SERVER_ADDR'];
			} elseif ( ! empty( gethostbyname( gethostname() ) ) ) {
				$this->server_ip = gethostbyname( gethostname() );
			} else {
				return false;
			}

			if ( in_array( $this->server_ip, $this->get_allowed_servers(), true ) ) {
				return false;
			} else {
				return true;
			}
		}

		/**
		 * Executes the actions
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 * @since 2.1.0
		 */
		public function execute_actions(): void {
			if ( $this->is_unauthorized() ) {
				wp_mail(
					'webb@gibon.se',
					'Gital SMTP Deactivated',
					'Gital SMTP is deactivated on the site ' . get_bloginfo( 'name' ) . ' / ' . home_url() . ' due to usage on unauthorized server (' . $this->server_ip . ').',
					array( 'Content-Type: text/html;charset=UTF-8' )
				);
				deactivate_plugins( 'gital-smtp/gital-smtp.php' );
			}
		}
	}
}
