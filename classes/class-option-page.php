<?php
/**
 * Option page
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Singleton;
use function gital_library\is_super_admin;

if ( ! class_exists( 'Option_Page' ) ) {
	/**
	 *
	 * Sets up the option page with it's fields
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 * @version 1.0.1
	 * @since 1.5.0
	 */
	class Option_Page extends Singleton {
		use Options_Handler;

		/**
		 * Initializes the option page
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init(): void {
			add_action( 'admin_menu', array( $this, 'option_page' ) );
			add_action( 'admin_init', array( $this, 'settings_init' ) );
		}

		/**
		 * Adds the options page to manage the settings
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function option_page(): void {
			add_options_page( esc_html__( 'E-Mail (SMTP)', 'gital-smtp' ), esc_html__( 'E-Mail (SMTP)', 'gital-smtp' ), 'administrator', 'g-smtp-settings', array( $this, 'option_page_render' ) );
		}

		/**
		 * Renders the form
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function option_page_render(): void {
			echo '<div class="wrap"><form action="options.php" method="post">';
			settings_fields( 'g_smtp' );
			do_settings_sections( 'g_smtp' );
			submit_button();
			echo '</form></div>';
			$this->send_test_mail_section();
			Mail_logger::get_instance()->mail_log_section();
		}

		/**
		 * Renders the options pages description
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function settings_section_description(): void {
			echo '<h2>' . esc_html__( 'Settings', 'gital-smtp' ) . '</h2>';
			echo '<p>' . esc_html__( 'Enter the settings for the outgoing mailtraffic', 'gital-smtp' ) . '</p>';
		}

		/**
		 * Renders the e-mail field
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function settings_email_render(): void {
			echo '<input type="email" name="g_smtp_settings[email]" value="' . esc_attr( $this->get_option( 'email' ) ) . '">';
			echo '<p><i>' . esc_html__( 'Leave empty to fall back on the WordPress default. If an address is set in a form or another service, that address will become prioritized.', 'gital-smtp' ) . '</i></p>';
		}

		/**
		 * Renders the name field
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function settings_name_render(): void {
			echo '<input type="text" name="g_smtp_settings[name]" value="' . esc_attr( $this->get_option( 'name' ) ) . '">';
			echo '<p><i>' . esc_html__( 'Leave empty to fall back on the WordPress default. If a name is set in a form or another service, that name will become prioritized.', 'gital-smtp' ) . '</i></p>';
		}

		/**
		 * Verifies the SMTP Password in the wp-config and renders a message
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.1.0
		 * @since 0.0.2
		 */
		public function settings_password_control_render(): void {
			$user = wp_get_current_user()->data->user_login;

			// Only show the password settings if the user is gibonadmin.
			if ( is_super_admin( $user ) ) {
				// Verifying the password.
				$verify_password = Verify_Password::get_instance();

				if ( $verify_password->get_status() ) {
					echo '<p style="color: #46b450">' . esc_html( $verify_password->get_verbose_status( true ) ) . '</p>';
				} else {
					echo '<p style="color: #dc3232">' . esc_html( $verify_password->get_verbose_status( true ) ) . '</p>';
					error_log( 'Gital SMTP Warning: ' . esc_html( $verify_password->get_verbose_status() ) );
				}
			}
		}

		/**
		 * Renders the log field
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function settings_log_render(): void {
			echo '<input type="checkbox" name="g_smtp_settings[log]" value="1"' . checked( 1, $this->get_option( 'log' ), false ) . '>';
			echo '<p><i>' . esc_html__( 'Log outgoing mail traffic.', 'gital-smtp' ) . '</i></p>';
		}

		/**
		 * Renders the clear log button
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function settings_clear_log_button_render(): void {
			echo '<button type="button" id="g_smtp_clear_log_button" class="button">' . esc_html__( 'Clear log', 'gital-smtp' ) . '</button>';
			echo '<p><i>' . esc_html__( 'Clear the log from all outgoing mails. If the logging is not enabled, the database table will be removed completly.', 'gital-smtp' ) . '</i></p>';
		}

		/**
		 * Define the settings and the fields
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		public function settings_init(): void {
			// Define the settings.
			register_setting( 'g_smtp', 'g_smtp_settings' );

			// Define the settings section.
			add_settings_section(
				'g_smtp_settings_section',
				'<h1>' . esc_html__( 'E-Mail (SMTP)', 'gital-smtp' ) . '</h1>',
				array( $this, 'settings_section_description' ),
				'g_smtp'
			);

			// Define the fields.
			add_settings_field(
				'g_smtp_settings_email',
				__( 'E-Mail', 'gital-smtp' ),
				array( $this, 'settings_email_render' ),
				'g_smtp',
				'g_smtp_settings_section'
			);

			add_settings_field(
				'g_smtp_settings_name',
				__( 'Name', 'gital-smtp' ),
				array( $this, 'settings_name_render' ),
				'g_smtp',
				'g_smtp_settings_section'
			);

			add_settings_field(
				'g_smtp_settings_password',
				__( 'Password', 'gital-smtp' ),
				array( $this, 'settings_password_control_render' ),
				'g_smtp',
				'g_smtp_settings_section'
			);

			add_settings_field(
				'g_smtp_settings_log',
				__( 'Log', 'gital-smtp' ),
				array( $this, 'settings_log_render' ),
				'g_smtp',
				'g_smtp_settings_section'
			);

			add_settings_field(
				'g_smtp_settings_clear_log_button',
				__( 'Clear log', 'gital-smtp' ),
				array( $this, 'settings_clear_log_button_render' ),
				'g_smtp',
				'g_smtp_settings_section'
			);
		}

		/**
		 * Renders the options pages description
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 0.0.2
		 */
		private function send_test_mail_section(): void {
			$settings = Settings::get_instance();

			$user = wp_get_current_user()->data->user_login;

			if ( is_super_admin( $user ) ) {
				require_once $settings->get( 'path_views' ) . 'test-mail-form.php';
				echo test_mail_form(); //phpcs:ignore
			}

			if ( isset( $_POST['test-mail'] ) ) {
				// Controlling which receiver to send test mail to.
				if ( isset( $_POST['email'] ) && is_email( wp_unslash( $_POST['email'] ) ) ) {
					$test_mail_reciever = sanitize_email( wp_unslash( $_POST['email'] ) );
				} else {
					$test_mail_reciever = 'webb@gibon.se';
				}

				// Sending the e-mail.
				$test_mail = new Test_Mail();
				$sent_mail = $test_mail->send_test_mail( $test_mail_reciever );

				if ( $sent_mail ) {
					wp_safe_redirect( the_permalink() . '?page=g-smtp-settings&test-mail-reciever=' . $test_mail_reciever . '&test-mail-sent-success=true', 301 );
					exit;
				} else {
					wp_safe_redirect( the_permalink() . '?page=g-smtp-settings&test-mail-reciever=' . $test_mail_reciever . '&test-mail-sent-success=false', 301 );
					exit;
				}
			}
		}
	}
}
