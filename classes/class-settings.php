<?php
/**
 * Settings
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Singleton;
use gital_library\Settings_Handler;

if ( ! class_exists( 'Settings' ) ) {
	/**
	 * Settings
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 5.0.0
	 */
	class Settings extends Singleton {
		use Settings_Handler;

		/**
		 * Init
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init() {
			$this->define_settings();
		}

		/**
		 * Define settings
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		private function define_settings(): void {
			// Environments.
			$this->set( 'url_root', plugins_url( '', __DIR__ ) );
			$this->set( 'url_assets', $this->get( 'url_root' ) . '/assets' );
			$this->set( 'path_root', plugin_dir_path( __DIR__ ) );
			$this->set( 'path_functions', $this->get( 'path_root' ) . 'functions/' );
			$this->set( 'path_views', $this->get( 'path_root' ) . 'views/' );
			$this->set( 'path_vendor', $this->get( 'path_root' ) . 'vendor/' );

			// Settings.
			$this->set( 'password_hashed', '$2y$10$IP3fvMp5BPBEppd9vv4vaeJm2iS9oPTPZGoB4WxjJPUN0yYhSYEzC' );
		}
	}
}
