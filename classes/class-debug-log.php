<?php
/**
 * Debug Log
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Singleton;

if ( ! class_exists( 'Debug_Log' ) ) {
	/**
	 * Debug log
	 *
	 * Logs debug messages from phpmailer
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 * @since 1.6.0
	 */
	class Debug_Log extends Singleton {
		/**
		 * Initializes the debug log
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init(): void {
			add_action( 'wp_mail_failed', array( $this, 'debug_log' ), 10, 1 );
		}

		/**
		 * Activates the debug log
		 *
		 * @param \WP_Error $wp_error The WP_Error object.
		 *
		 * @return \WP_Error The WP_Error object
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 * @since 1.6.0
		 */
		public function debug_log( \WP_Error $wp_error ): \WP_Error {
			error_log( print_r( $wp_error, true ) );
			return $wp_error;
		}
	}
}
