<?php
/**
 * Password Handler
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

if ( ! class_exists( 'Password_Handler' ) ) {
	/**
	 * Password Handler
	 *
	 * Encrypts and Decrypts the SMTP-password
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.1
	 * @since 4.0.0
	 */
	class Password_Handler {
		/**
		 * Get password
		 *
		 * @return string The password
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function get_password(): string {
			if ( ! defined( 'SMTP_PASS' ) ) {
				return '';
			}

			$password = get_option( 'g_smtp_password' );
			return ! empty( $password ) ? $this->decrypt( $password ) : '';
		}

		/**
		 * Set password
		 *
		 * @param string $password The password to set.
		 *
		 * @return string The status
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function set_password( string $password ) {
			if ( ! defined( 'SMTP_PASS' ) ) {
				return '';
			}

			$encrypted_password = $this->encrypt( $password );
			return update_option( 'g_smtp_password', $encrypted_password, false ) ? 'Password set' : 'Password not set';
		}

		/**
		 * Encrypt password
		 *
		 * @param string $password The password to encrypt.
		 * @param string $cipher The cipher to use.
		 *
		 * @return string The encrypted password
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		protected function encrypt( string $password, string $cipher = 'AES-128-CBC' ): string {
			if ( ! defined( 'SMTP_PASS' ) ) {
				return '';
			}

			$key            = openssl_digest( SMTP_PASS, 'SHA256', true );
			$ivlen          = openssl_cipher_iv_length( $cipher );
			$iv             = openssl_random_pseudo_bytes( $ivlen );
			$ciphertext_raw = openssl_encrypt( $password, $cipher, $key, OPENSSL_RAW_DATA, $iv );
			$hmac           = hash_hmac( 'sha256', $ciphertext_raw, $key, true );

			return base64_encode( $iv . $hmac . $ciphertext_raw );
		}

		/**
		 * Decrypt
		 *
		 * @param string $encrypted_password The encrypted password.
		 * @param string $cipher The cipher to use.
		 *
		 * @return string The decrypted password.
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		protected function decrypt( string $encrypted_password, string $cipher = 'AES-128-CBC' ): string {
			if ( ! defined( 'SMTP_PASS' ) ) {
				return '';
			}

			$c              = base64_decode( $encrypted_password );
			$key            = openssl_digest( SMTP_PASS, 'SHA256', true );
			$ivlen          = openssl_cipher_iv_length( $cipher );
			$iv             = substr( $c, 0, $ivlen );
			$hmac           = substr( $c, $ivlen, $sha2len = 32 );
			$ciphertext_raw = substr( $c, $ivlen + $sha2len );
			$password       = openssl_decrypt( $ciphertext_raw, $cipher, $key, OPENSSL_RAW_DATA, $iv );

			$calcmac = hash_hmac( 'sha256', $ciphertext_raw, $key, true );

			if ( hash_equals( $hmac, $calcmac ) ) {
				return $password;
			}
		}
	}
}
