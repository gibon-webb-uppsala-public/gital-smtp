<?php
/**
 * Mail init
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Singleton;

if ( ! class_exists( 'Mail_Init' ) ) {
	/**
	 * Initiates, sets up the authentication, host and sender for the mail
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.3.1
	 * @since 1.5.0
	 */
	class Mail_Init extends Singleton {
		use Options_Handler;

		/**
		 * Initializes the mail
		 *
		 * @return void
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public function init(): void {
			add_action( 'phpmailer_init', array( $this, 'smtp_init' ), 20 );
		}

		/**
		 * Sets up the authentication, host and sender for the mail
		 *
		 * @param \PHPMailer\PHPMailer\PHPMailer $mail The PHPMailer object.
		 *
		 * @return \PHPMailer\PHPMailer\PHPMailer The PHPMailer object
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.2.1
		 * @since 0.0.2
		 */
		public function smtp_init( \PHPMailer\PHPMailer\PHPMailer $mail ): \PHPMailer\PHPMailer\PHPMailer {

			// Verifying the password.
			if ( ! Verify_Password::get_status() ) {
				return $mail;
			}

			// Get the password.
			$password_handler = new Password_Handler();
			$password         = $password_handler->get_password();

			// Setting up variables.
			$mail->isSMTP();
			$mail->Host       = '195.198.1.11';
			$mail->SMTPAuth   = true;
			$mail->Port       = 587;
			$mail->Username   = 'gibonwebb@kundmail.narproduceradit.net';
			$mail->Password   = $password;
			$mail->SMTPSecure = false;

			// Is it the default address that is sending the mail or the WordPress admin.
			$current_address    = strtolower( $mail->From );
			$is_default_address = false;

			// Check if the address is the default one.
			if ( str_contains( $current_address, 'wordpress' ) !== false || strtolower( get_option( 'admin_email' ) ) === $current_address ) { // phpcs:ignore
				$is_default_address = true;
			}

			// Then, override the sender e-mail.
			if ( ! empty( $this->get_option( 'email' ) ) && $is_default_address && is_email( $this->get_option( 'email' ) ) ) {
				$mail->From = $this->get_option( 'email' );
			} elseif ( $is_default_address ) {
				$mail->From = 'wordpress@' . str_replace( array( 'http://', 'https://', '/wp', 'www.' ), '', get_bloginfo( 'wpurl' ) );
			}

			// Is it the default from name that is sending the mail or the site name.
			$current_name    = strtolower( $mail->FromName );
			$is_default_name = false;

			// Check if the name is the default one.
			if ( strtolower( get_bloginfo( 'name' ) ) === $current_name || 'wordpress' === $current_name ) { // phpcs:ignore
				$is_default_name = true;
			}

			// Then, override the sender name.
			if ( ! empty( $this->get_option( 'name' ) ) && $is_default_name ) {
				$mail->FromName = $this->get_option( 'name' );
			} elseif ( $is_default_name ) {
				$mail->FromName = get_bloginfo( 'name' );
			}

			if ( 1 == $this->get_option( 'log' ) ) {
				Mail_Logger::get_instance()->add_log_entry( $mail );
			}

			return $mail;
		}
	}
}
