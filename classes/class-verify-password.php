<?php
/**
 * Debug Log
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use gital_library\Singleton;

if ( ! class_exists( 'Verify_Password' ) ) {
	/**
	 * Verify_Password
	 *
	 * Verifies the password
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.1.3
	 * @since 1.7.0
	 */
	class Verify_Password extends Singleton {
		/**
		 * Get verbose status
		 *
		 * @param boolean $translated True if the return string should be translated.
		 *
		 * @return string The status
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.0
		 */
		public static function get_verbose_status( bool $translated = false ): string {
			$settings = Settings::get_instance();

			if ( ! defined( 'SMTP_PASS' ) ) {
				return $translated ? esc_html__( 'The password key is not set', 'gital-smtp' ) : 'The password key is not set';
			}

			if ( ! get_option( 'g_smtp_password' ) ) {
				return $translated ? esc_html__( 'The password is not set', 'gital-smtp' ) : 'The password is not set';
			}

			$password_handler = new Password_Handler();
			$password         = $password_handler->get_password();

			if ( password_verify( $password, $settings->get( 'password_hashed' ) ) ) {
				return $translated ? esc_html__( 'The password is set and correct', 'gital-smtp' ) : 'The password is set and correct';
			} else {
				return $translated ? esc_html__( 'The password is set but not correct', 'gital-smtp' ) : 'The password is set but not correct';
			}
		}

		/**
		 * Get status
		 *
		 * @return bool The status
		 *
		 * @author Gustav Gesar <gustav.gesar@gibon.se>
		 *
		 * @version 1.0.1
		 */
		public static function get_status(): bool {
			$settings = Settings::get_instance();

			$password_handler = new Password_Handler();
			$password         = $password_handler->get_password();

			if ( defined( 'SMTP_PASS' ) && ! empty( SMTP_PASS ) && get_option( 'g_smtp_password' ) && password_verify( $password, $settings->get( 'password_hashed' ) ) ) {
				return true;
			} else {
				return false;
			}
		}
	}
}
