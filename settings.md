<!-- Bedrock Config -->

Config::define('SMTP_PASS', env('SMTP_PASS'));

<!-- Bedrock Env -->

SMTP_PASS=REPLACE_WITH_KEY

<!-- Vanilla Config -->

define('SMTP_PASS', "REPLACE_WITH_KEY";
