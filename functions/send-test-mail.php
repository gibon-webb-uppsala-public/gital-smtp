<?php
/**
 * Send test mail
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

/**
 * Send test mail
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @param string $address The address to send the mail to.
 *
 * @version 1.0.0
 * @since 1.5.0
 */
function send_test_mail( $address = 'webb@gibon.se' ) {
	$test_mail = new Test_Mail();
	$test_mail->send_test_mail( $address );

	if ( $test_mail ) {
		return 'Your test mail has been sent';
	} else {
		return 'Your test mail has not been sent';
	}
}
