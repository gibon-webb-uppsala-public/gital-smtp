<?php
/**
 * Set password
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

/**
 * Set password
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @param string $password The password to be set.
 *
 * @version 1.0.0
 * @since 4.0.0
 */
function set_password( $password ) {
	$password_handler = new Password_Handler();
	$response         = $password_handler->set_password( $password );

	return $response;
}
