<?php
/**
 * Options Handler
 *
 * @package Gital SMTP
 */

namespace gital_smtp;

use function gital_library\parse_args;

/**
 * Options_Handler
 *
 * @author Gustav Gesar <gustav.gesar@gibon.se>
 *
 * @version 1.0.0
 */
trait Options_Handler {
	/**
	 * Options
	 *
	 * @var array $options The options
	 */
	protected $options;

	/**
	 * Gets the options
	 *
	 * @return array The options
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	protected function get_options() {
		if ( ! isset( $this->options ) ) {
			$this->options = get_option( 'g_smtp_settings' );
		}

		$this->options = $this->upgrade_options( $this->options ); // TODO Remove this function in the future.

		$defaults = array(
			'email' => '',
			'name'  => '',
			'log'   => 0,
		);

		$this->options = parse_args( $this->options, $defaults );
		return $this->options;
	}

	/**
	 * Upgrades the options.
	 *
	 * @param array $options The options.
	 *
	 * @return array The upgraded options
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.0
	 */
	protected function upgrade_options( $options ) {
		if ( ! is_array( $options ) ) {
			return $options;
		}

		$old_keys = array(
			'g_smtp_settings_email',
			'g_smtp_settings_name',
			'g_smtp_settings_log',
		);

		if ( array_intersect( array_keys( $options ), $old_keys ) ) {
			$options = array(
				'email' => $options['g_smtp_settings_email'] ?? '',
				'name'  => $options['g_smtp_settings_name'] ?? '',
				'log'   => $options['g_smtp_settings_log'] ?? 0,
			);

			update_option( 'g_smtp_settings', $options );
		}

		return $options;
	}

	/**
	 * Gets an option
	 *
	 * @param string $option The option.
	 *
	 * @return mixed The option
	 *
	 * @author Gustav Gesar <gustav.gesar@gibon.se>
	 *
	 * @version 1.0.1
	 */
	protected function get_option( $option ) {
		$options = $this->get_options();
		if ( isset( $options[ $option ] ) ) {
			return $options[ $option ];
		}
		return false;
	}
}
